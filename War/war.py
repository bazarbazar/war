import logging

import cards

log = logging.getLogger("MainLog.war")


class War(cards.Cards):


	player1=[]
	player2=[]


	def first_half_deck(self):
		player1 = self.deck[0:int(len(self.deck) / 2)]
		self.player1=player1
		log.debug('1st half of deck')



	def second_half_deck(self):
		player2 = self.deck[int(len(self.deck) / 2):]
		self.player2=player2
		log.debug('2nd half of deck')

	def check_winner(self):

		if ((len(self.player1) == 0) and (len(self.player2) == 0)):
			return 3
		elif len(self.player1) == 0:
			return 2
		elif len(self.player2) == 0:
			return 1
		else:
			return 0

	def play2cards(self):
		if ((len(self.player1) != 0) and (len(self.player2) != 0)):


			self.table.insert(0, self.player1[0])
			self.table.insert(0, self.player2[0])
			log.debug("player1 deck: " + str(self.player1))
			log.debug("player2 deck: " + str(self.player2))
			log.debug("player1: " + str(self.player1[0]))

			log.debug("palyer2: " + str(self.player2[0]))

			log.debug("zagranie 2 kart")
			self.player1.pop(0)
			self.player2.pop(0)

			return 0
		else:
			return 1

	def battle_win(self, player):
		player += (self.table)
		return player

	def war(self):

		while True:
			log.debug("Wojna")
			if (self.play2cards() == 0):

				if ((len(self.player1) != 0) and (len(self.player2) != 0)):

					if self.player1[0] != self.player2[0]:
						log.debug("koniec wojny")

					if ((self.player1[0] > self.player2[0])):
						self.play2cards()
						self.player1 += self.table
						log.debug("wojna dla gracz1")
						log.debug(self.player1)
						log.debug(self.player2)
						break

					if ((self.player1[0] < self.player2[0])):
						self.play2cards()
						self.player2 += self.table
						log.debug("wojna dla gracz2")
						log.debug(self.player1)
						log.debug(self.player2)
						break

					elif ((len(self.player1) == 0) or (len(self.player2) == 0)):
						break
			else:
				break

	def play(self):

		if (self.check_winner() == 0):

			if self.player1[0] > self.player2[0]:
				self.play2cards()
				self.player1 += self.table
				log.debug("zbiera gracz 1")
				log.debug(self.player1)
				self.clear_table()

			elif self.player1[0] < self.player2[0]:
				self.play2cards()
				self.player2 += (self.table)
				log.debug("zbiera gracz 2")
				log.debug(self.player2)
				self.clear_table()

			else:
				if (self.play2cards() == 0):
					self.war()

					self.clear_table()
				else:
					log.info("gra")
		elif (self.check_winner() == 1):
			log.info("player1 win")
			return 1
		elif self.check_winner() == 2:
			log.info("player2 win")
			return 2
		elif self.check_winner() == 3:
			log.info("remis")
			return 3


