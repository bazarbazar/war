import unittest
import war
import logging
log = logging.getLogger("MainLog.test_war")


class CardsTestCase(unittest.TestCase):

    def test_check_if_first_half_deck_equals_half_deck(self):
        deck = war.War()
        deck.first_half_deck()
        self.assertEqual(len(deck.player1),len(deck.deck)/2)

    def test_check_if_second_half_deck_equals_half_deck(self):
        deck = war.War()
        deck.second_half_deck()
        self.assertEqual(len(deck.player2),len(deck.deck)/2)

    def test_check_if_check_winner_equals_3_remis(self):
        game =war.War()
        self.assertEqual(game.check_winner(),3)

    def test_check_if_check_winner_equals_1_player1_winner(self):
        game =war.War()
        game.player1=[1,2,3]
        self.assertEqual(game.check_winner(),1)

    def test_check_if_check_winner_equals_2_player2_winner(self):
        game =war.War()
        game.player2=[1,2,3]
        self.assertEqual(game.check_winner(),2)