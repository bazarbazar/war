import logging
import module_test
import war

log = logging.getLogger("MainLog")
log.setLevel(logging.INFO)

ch = logging.FileHandler(filename="main_log.log")
format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ch.setFormatter(format)
log.addHandler(ch)



def main():

	game = war.War()
	game.shuffle_deck()
	game.shuffle_deck()
	game.shuffle_deck()
	game.shuffle_deck()
	game.shuffle_deck()
	game.shuffle_deck()

	game.first_half_deck()

	game.second_half_deck()
	print(game.deck)
	infinite_flag = 0
	while True:
		infinite_flag+=1
		print(infinite_flag)

		match = game.play()
		if match==1:
			break
		elif match ==2:
			break
		elif infinite_flag>=50000:
			log.info("Infinite match")
			break
#3562
#3747

main()
