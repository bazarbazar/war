import unittest
import cards
import logging
log = logging.getLogger("MainLog.test_cards")


class CardsTestCase(unittest.TestCase):
    def test_check_if_deck_not_empty(self):
        res = cards.Cards()
        self.assertGreater(len(res.deck),0)


    def test_check_if_deck_equal_54(self):
        res = cards.Cards()
        self.assertEqual(len(res.deck), 54)



    def test_clear_table(self):
        test = [1,2,3]
        deck = cards.Cards()
        deck.set_table(test)

        log.debug(deck.get_table())
        deck.clear_table()

        self.assertEqual(len(deck.table), 0)