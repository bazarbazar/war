import random
import logging


log = logging.getLogger("MainLog.cards")


class Cards(object):




	def __init__(self):
		self.deck = [2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10,
					 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15]


		log.debug("Cards init")




	table = []


	def shuffle_deck(self):
		random.shuffle(self.deck)

		log.debug('shuffled deck')

	def clear_table(self):
		self.table = []
		log.debug('table cleared')

	def set_table(self, table):
		self.table = table

	def get_table(self):
		return self.table


